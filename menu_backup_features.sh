#!/usr/bin/env bash
# bash -x
##############################
# Created by: Theraphosid
# Choose your fighter and execute your choise
# Date: 20.10.2020
##############################

Clear_swap(){
swapoff -a
sleep 1
swapon -a
}

Back_up_and_zip(){
if Check_partition_existence $0 ;then
	echo "success backing you up and ziping"
	sudo dd if=/dev/$disk_name|gzip -c > disk_back_up.gz
else
	echo "failed to back up and zip"
	return 1
fi
}

Back_up_mbr(){
if Check_partition_existence $0 ;then
	echo "success backing up your mbr"
	sudo dd if=/dev/$disk_name of=/home/mbr_$(date +%Y\%m\%d).img count=1 bs=512
else
	echo "failed to copy mbr"
	return 1
fi
}

Back_up(){
if Check_partition_existence $0 ;then
	echo "success backing up your partition"
	sudo dd if=/dev/$disk_name of=/home/partition_backup$(date +%Y\%m\%d).img
else
	echo "failed to copy home partition"
	return 1
fi
}


Check_partition_existence(){
result=$(fdisk -l|grep -v100 -i Device|grep /dev/|grep -v Disk|cut -d '/' -f3|cut -d ' ' -f1|grep $disk_name)
echo $result
if [[ $results -ne $disk_name ]];then
	return 1
else
	Mount_check
	return 0
fi
}

Mount_check(){
check=$(mount|grep $disk_name;echo $?)
if [[ $check -ne 0 ]];then
	echo "not mounted"
else
	echo "mounted,unmounting the disk"
	umount $disk_name
fi
}




while getopts ":abcdef" option;do
case $option in
a)
echo "received -a executing your option"
read -p "insert disk name :" disk_name
if Back_up_mbr $0 ;then
	echo "success"
else
	echo "fail"
fi
;;
b)
echo "received -b executing your option"
read -p "insert home partition disk name :" disk_name
if Back_up $0 ;then
	echo "success"
else
	echo "fail"
fi
;;
c)
echo "received -c executing your option"
read -p "insert home partition disk name for a back up zip file:" disk_name
if Back_up_and_zip $0 ;then
	echo "success"
else
	echo "fail"
fi
;;
d)
echo "received -d executing your option"
read -p "insert root partition disk name for a backed up zip file:" disk_name
if Back_up_and_zip $0 ;then
	echo "success"
else
	echo "fail"
fi
;;
e)
echo "received -e executing your option"
read -p "insert disk name  for whole disk backup zip file: " disk_name
if Back_up_and_zip $0 ;then
	echo "success"
else
	echo "fail"
fi
;;
f)
echo "received -f executing your option"
Clear_swap
echo "swap has been cleand"
;;
esac
done
