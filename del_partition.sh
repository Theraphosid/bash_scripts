#!/usr/bin/env bash
#Written by: Theraphosid
#taking a rquired disk name and deleting all curent partitions.


delete_part(){
read -p "Please enter the full path to the disk you want to delete: " path_disk
read -p "Enter name of the disk: " disk
read -p "Enter the amount of partitions you want to delete from the disk: " i
echo "Checking if the disk is mounted..."
sleep 3

mount_check=mount|grep $path_disk|cut -d" " -f1:echo $?

if [[ $mount_check -gt 0 ]];then
	printf "The disk $path_disk is mounted\n"
	printf "Making preperations for deleting the disks you choose.\n"
	sleep 5
	sed -i "/$disk/d" /etc/fstab
	while [[ $i -gt 0 ]];do
		fdisk $path_disk << EOF
d

w
EOF
let i--
done

else
	printf "The disk $disk isn't mounted we can proside.\n"
	while [[ $i -gt 0 ]];do
		fdisk $path_disk << EOF
d

w
EOF
let i--
done
fi
}
delete_part $@
