#!/usr/bin/env bash
#Written by: Theraphosid
#Takeing a required disk name and creating 2 primary partitions 1G size each

create_part(){
counter=2
read -p "Please provide a full path to the disk: " disk_path
while [[ $counter -gt 0 ]];do
	fdisk $disk_path << EOF
n
p


+1G
w
EOF
let counter--
done
}
create_part
